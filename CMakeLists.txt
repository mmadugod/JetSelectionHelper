# The name of the package
ATLAS_SUBDIR(JetSelectionHelper)

ATLAS_ADD_LIBRARY ( JetSelectionHelperLib JetSelectionHelper/JetSelectionHelper.h src/JetSelectionHelper.cxx
		  PUBLIC_HEADERS JetSelectionHelper
		  LINK_LIBRARIES xAODEventInfo
				 xAODRootAccess
				 xAODJet)
